use serde::Deserialize;
use std::sync::mpsc::{Receiver, Sender};

use casual_logger::Log;

fn decode_base64(data: &str) -> String {
    use base64::Engine;

    String::from_utf8_lossy(
        &base64::engine::general_purpose::STANDARD
            .decode(data)
            .expect("python side must encode it correctly"),
    )
    .into_owned()
}

pub struct GdbWrapper {
    gdb: gdb::Debugger,
    command_receiver: Receiver<GdbRequest>,
    response_sender: Sender<GdbResponse>,
}

#[derive(Debug)]
pub enum GdbLog {
    Request(String),
    Response(String),
}

#[derive(Debug)]
pub enum GdbRequest {
    OpenExecutable(String), // filename
    Custom(String),         // execute custom gdb command
    BreakpointSet((String, i32)),
    BreakpointDelete((String, i32)),
    BreakpointToggle((i32, bool)),
    BreakpointsDeleteAll,
    BreakpointsToggle(bool),
    WaitForExecution,
    Run,
    Continue,
    Next,
    Step,
    GetLocals,
    SelectFrame(i32, i32),
}

#[derive(Debug, Deserialize)]
pub struct MainFunction {
    pub filename: String,
    pub line: i32,
}

#[derive(Debug)]
pub enum GdbResponse {
    Log(GdbLog),
    OpenedExecutable(MainFunction), // source filename with the main function in it
    Breakpoints(Vec<Breakpoint>),
    Running,
    Stopped(Vec<Thread>),
    Panicing,
    Values(serde_json::Value),
}

#[derive(Debug, Deserialize, Clone)]
pub struct Thread {
    pub name: String,
    pub id: i32,
    pub tid: i32,
    pub frames: Vec<Frame>,
    pub is_current: bool,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Frame {
    pub function: Option<String>,
    pub filename: String,
    pub line: i32,
    pub level: i32,
    pub pc: i64,
    pub is_current: bool,
    pub is_library: bool,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Breakpoint {
    pub number: i32,
    pub filename: String,
    pub line: i32,
    pub enabled: bool,
}

impl GdbWrapper {
    pub fn new(
        gdb: gdb::Debugger,
        command_receiver: Receiver<GdbRequest>,
        response_sender: Sender<GdbResponse>,
    ) -> Self {
        GdbWrapper {
            gdb,
            command_receiver,
            response_sender,
        }
    }

    /// make sure this is called from a separate thread because it will block
    pub fn start(&mut self) {
        loop {
            if let Ok(command) = self.command_receiver.try_recv() {
                match command {
                    GdbRequest::BreakpointSet((filename, line)) => {
                        self.send_command(&format!("-my-break-set {}:{}", filename, line));
                    }
                    GdbRequest::BreakpointDelete((filename, line)) => {
                        self.send_command(&format!("-my-break-delete {}:{}", filename, line));
                    }
                    GdbRequest::BreakpointsDeleteAll => {
                        self.send_command("-my-breaks-delete");
                    }
                    GdbRequest::BreakpointsToggle(status) => {
                        let status = if status { "on" } else { "off" };
                        self.send_command(&format!("-my-breaks-toggle {}", status));
                    }
                    GdbRequest::BreakpointToggle((id, status)) => {
                        let status = if status { "on" } else { "off" };
                        self.send_command(&format!("-my-break-toggle {} {}", id, status));
                    }
                    GdbRequest::OpenExecutable(file) => {
                        self.open_executable(&file);
                    }
                    GdbRequest::WaitForExecution => {
                        self.wait_for_execution();
                    }
                    GdbRequest::Custom(command) => {
                        self.send_command(&command);
                    }
                    GdbRequest::Run => {
                        self.send_command("run");
                    }
                    GdbRequest::Continue => {
                        self.send_command("continue");
                    }
                    GdbRequest::Next => {
                        self.send_command("next");
                    }
                    GdbRequest::Step => {
                        self.send_command("step");
                    }
                    GdbRequest::GetLocals => {
                        self.send_command("-my-locals");
                    }
                    GdbRequest::SelectFrame(thread_id, level) => {
                        self.send_command(&format!("thread {}", thread_id));
                        self.send_command(&format!("frame {}", level));
                        self.send_command("-my-locals");
                    }
                }
            }
        }
    }

    fn panic(&mut self, message: &str) -> ! {
        self.response_sender
            .send(GdbResponse::Panicing)
            .unwrap_or_else(|err| self.panic(&err.to_string()));

        Log::error(message);
        panic!("{}", message);
    }

    fn wait_for_execution(&mut self) {
        let records = self
            .gdb
            .read_sequence()
            .unwrap_or_else(|err| self.panic(&err.to_string()));
        self.process_records(records);
    }

    fn send_command(&mut self, command: &str) -> Vec<gdb::Record> {
        self.response_sender
            .send(GdbResponse::Log(GdbLog::Request(command.to_string())))
            .unwrap_or_else(|err| self.panic(&err.to_string()));

        let records = self
            .gdb
            .send_cmd_raw(command)
            .unwrap_or_else(|err| self.panic(&err.to_string()));

        self.process_records(records)
    }

    fn process_records(&mut self, records: Vec<(gdb::Record, String)>) -> Vec<gdb::Record> {
        records
            .into_iter()
            .map(|(record, log)| {
                self.response_sender
                    .send(GdbResponse::Log(GdbLog::Response(log.clone())))
                    .unwrap_or_else(|err| self.panic(&err.to_string()));
                // Log::info(&format!("raw response: {}", log));

                match &record {
                    /*
                    gdb::Record::Async(gdb::AsyncRecord::Exec(record)) => {
                    }
                    */
                    gdb::Record::Stream(gdb::StreamRecord::Console(console)) => {
                        // Log::info(&format!("stream: {:#?}", &console));
                        if let Some((command, body)) = console.split_once(' ') {
                            if command == "stopped" {
                                self.process_frames(body);
                            }
                        }
                    }
                    gdb::Record::Result(result) => {
                        if result.class == gdb::ResultClass::Running {
                            self.response_sender.send(GdbResponse::Running).unwrap();
                        } else {
                            result.content.iter().for_each(|var| match &var.name[..] {
                                "locals" => {
                                    self.process_locals(&var.value);
                                }
                                "main_function" => {
                                    self.find_main_file(&var.value);
                                }
                                "breakpoints" => {
                                    self.process_breakpoints(&var.value);
                                }
                                _ => {}
                            });
                        }
                    }
                    _ => {}
                }

                // Log::debug(&format!("{:#?}", record));

                record
            })
            .collect()
    }

    fn process_locals(&mut self, locals: &gdb::Value) {
        match locals {
            gdb::Value::String(string) => {
                match serde_json::from_str::<serde_json::Value>(&decode_base64(string)) {
                    Ok(v) => {
                        // Log::info(&format!("{:?}", v));
                        self.response_sender.send(GdbResponse::Values(v)).unwrap();
                    }
                    Err(e) => {
                        Log::error(&format!("Can't parse locals: {}", e));
                    }
                }
            }
            _ => self.panic(&format!("expect locals to be a string, got: {:?}", locals)),
        }
    }

    fn process_frames(&mut self, frames: &str) {
        match serde_json::from_str::<Vec<Thread>>(&decode_base64(frames)) {
            Ok(f) => {
                // Log::info(&format!("frames: {:?}", f));
                self.response_sender.send(GdbResponse::Stopped(f)).unwrap();
            }
            Err(e) => {
                Log::error(&format!("Can't parse frames: {}", e));
            }
        }
    }

    fn process_breakpoints(&mut self, breakpoints: &gdb::Value) {
        match breakpoints {
            gdb::Value::String(string) => {
                match serde_json::from_str::<Vec<Breakpoint>>(&decode_base64(string)) {
                    Ok(b) => {
                        // Log::info(&format!("{:?}", b));
                        self.response_sender
                            .send(GdbResponse::Breakpoints(b))
                            .unwrap();
                    }
                    Err(e) => {
                        Log::error(&format!("Can't parse breakpoints: {}", e));
                    }
                }
            }
            _ => self.panic(&format!(
                "expect breakpoints to be a string, got: {:?}",
                breakpoints
            )),
        }
    }

    fn find_main_file(&mut self, main_function: &gdb::Value) {
        match main_function {
            gdb::Value::String(string) => {
                match serde_json::from_str::<MainFunction>(&decode_base64(string)) {
                    Ok(f) => {
                        self.response_sender
                            .send(GdbResponse::OpenedExecutable(f))
                            .unwrap();
                    }
                    Err(e) => {
                        Log::error(&format!("Can't parse main function data: {}", e));
                    }
                }
            }
            _ => self.panic(&format!(
                "expect main function to be a string, got: {:?}",
                main_function
            )),
        }
    }

    fn open_executable(&mut self, executable: &str) {
        self.send_command(&format!("file {}", executable));
        self.send_command("-my-find-main-func");
    }
}
