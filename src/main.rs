#![warn(clippy::all, rust_2018_idioms)]

mod app;
mod gdb_wrapper;

use std::process::exit;

use casual_logger::Log;
use gdb_wrapper::{GdbRequest, GdbResponse, GdbWrapper};
use std::io::Write;

fn main() {
    let script: String = String::from_utf8_lossy(include_bytes!("../script.py")).into_owned();

    let debugger = match gdb::Debugger::start() {
        Ok(debugger) => debugger,
        Err(e) => {
            eprintln!("Can't connect to gdb: {}", e);
            std::process::exit(1);
        }
    };

    let (command_sender, command_receiver) = std::sync::mpsc::channel::<GdbRequest>();
    let (response_sender, response_receiver) = std::sync::mpsc::channel::<GdbResponse>();

    Log::set_file_name("logs");
    Log::remove_old_logs();

    std::thread::spawn(move || {
        GdbWrapper::new(debugger, command_receiver, response_sender).start();
    });

    let mut file = match tempfile::Builder::default().suffix(".py").tempfile() {
        Ok(file) => file,
        Err(e) => {
            eprintln!("Couldn't create temp file for gdb scripts: {}", e);
            exit(1);
        }
    };

    file.write_all(script.as_bytes())
        .expect("Temp file should be writable");
    let filepath = file.path().to_str().expect("Temp file should have path");
    command_sender
        .send(GdbRequest::Custom(format!("source {}", filepath)))
        .unwrap();

    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "Debugger (WIP)",
        native_options,
        Box::new(|cc| Box::new(app::DebugApp::new(cc, command_sender, response_receiver))),
    );
}
