use std::{
    collections::HashMap,
    ffi::OsStr,
    fs,
    path::Path,
    sync::mpsc::{Receiver, Sender},
};

use casual_logger::Log;
use egui::{
    text::LayoutJob, CollapsingHeader, Color32, CursorIcon, Key, KeyboardShortcut, Label, Layout,
    Modifiers, Response, RichText, Rounding, Sense, Separator, Stroke, TextEdit, TextFormat, Ui,
    Vec2, Widget,
};
use egui_file::FileDialog;

use derivative::Derivative;
use serde_json::{Map, Value};

const ALT_1: KeyboardShortcut = KeyboardShortcut::new(Modifiers::ALT, Key::Num1);
// const ALT_2: KeyboardShortcut = KeyboardShortcut::new(Modifiers::ALT, Key::Num2);
const CTRL_O: KeyboardShortcut = KeyboardShortcut::new(Modifiers::CTRL, Key::O);

use crate::gdb_wrapper::{Breakpoint, GdbLog, GdbRequest, GdbResponse, Thread};

fn filename<S: AsRef<OsStr> + ?Sized>(string: &S) -> String {
    let path = Path::new(string);
    match path.file_name() {
        Some(name) => name.to_owned().into_string().unwrap(),
        None => "(unknown)".to_string(),
    }
}

fn render_complex_vars(ui: &mut Ui, fields: &Map<String, Value>) {
    for (index, (name, definition)) in fields.iter().enumerate() {
        let scalar = definition
            .get("scalar")
            .unwrap_or(&Value::Bool(false))
            .as_bool()
            .expect("Default value didn't work or something?");

        let name = format!("{}:", name);
        let var_type = match definition.get("type").expect("Type info must be there") {
            Value::String(var_type) => var_type,
            _ => "unknown type",
        };
        let address = match definition.get("address").unwrap_or(&Value::Null) {
            Value::String(address) => address,
            _ => "unknown address",
        };

        let hint = format!("{} ({})", var_type, address);

        if index > 0 {
            ui.separator();
        }

        if scalar {
            ui.horizontal(|ui| {
                ui.allocate_space([10.0, 0.0].into());

                ui.colored_label(Color32::GRAY, name).on_hover_text(&hint);

                let (color, value) = match definition
                    .get("value")
                    .expect("Value should be there. Check python script")
                {
                    Value::String(s) => (Color32::WHITE, format!("{:?}", s)),
                    Value::Number(n) => (Color32::WHITE, format!("{}", n)),
                    Value::Bool(b) => (Color32::WHITE, format!("{}", b)),
                    Value::Null => (Color32::WHITE, "null".to_string()),
                    _ => (Color32::RED, "Can't read value".to_string()),
                };

                ui.colored_label(color, value).on_hover_text(&hint);
            });
        } else {
            let mut job = LayoutJob::default();
            job.append(
                &name,
                0.0,
                TextFormat {
                    color: Color32::GRAY,
                    ..TextFormat::default()
                },
            );

            job.append(" ", 0.0, TextFormat::default());

            job.append(
                &hint,
                0.0,
                TextFormat {
                    color: Color32::DARK_GRAY,
                    ..TextFormat::default()
                },
            );
            CollapsingHeader::new(job)
                .default_open(false)
                .show(ui, |ui| {
                    if let Some(Value::Object(fields)) = definition.get("fields") {
                        render_complex_vars(ui, fields);
                    }
                });
        }
    }
}

pub struct SourceFile {
    lines: Vec<String>,
    scroll_offset: Vec2,
}

impl SourceFile {
    pub fn open(filename: String) -> Self {
        Self {
            lines: fs::read_to_string(filename)
                .expect("TODO: fix me at some point please")
                .lines()
                .map(|s| s.to_owned())
                .collect(),
            scroll_offset: Vec2::ZERO,
        }
    }
}

#[derive(Default)]
pub struct SourceFiles {
    all_files: HashMap<String, Option<SourceFile>>,
    opened_files: Vec<String>,
    current_filename: Option<String>,
}

impl SourceFiles {
    pub fn switch_to(&mut self, filename: &String) {
        self.all_files
            .entry(filename.clone())
            .or_insert_with(|| Some(SourceFile::open(filename.clone())));

        if !self.opened_files.contains(filename) {
            self.opened_files.push(filename.clone());
        }

        self.current_filename = Some(filename.clone());
    }
}

#[derive(Derivative)]
pub struct DebugApp {
    files: SourceFiles,
    custom_command: String,
    // line to scroll to and whether it was focused already
    focus_line: Option<(i32, bool)>,

    breakpoints: HashMap<String, Breakpoint>,

    values: serde_json::Value,
    threads: Vec<Thread>,
    current_frame: (i32, i32), // (thread index, frame index)

    locals_window_opened: bool,
    breakpoints_window_opened: bool,
    frames_window_opened: bool,

    #[derivative(Debug = "ignore")]
    open_file_dialog: Option<FileDialog>,
    gdb_logs: Vec<GdbLog>,
    gdb_logs_opened: bool,
    #[derivative(Debug = "ignore")]
    command_sender: Sender<GdbRequest>,
    #[derivative(Debug = "ignore")]
    response_receiver: Receiver<GdbResponse>,
}

impl DebugApp {
    /// Called once before the first frame.
    pub fn new(
        _cc: &eframe::CreationContext<'_>,
        command_sender: Sender<GdbRequest>,
        response_receiver: Receiver<GdbResponse>,
    ) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        // if let Some(storage) = cc.storage {
        //     return eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();
        // }

        let app = DebugApp {
            custom_command: String::new(),
            focus_line: Some((0, false)),

            files: SourceFiles::default(),

            values: serde_json::Value::Null,
            threads: vec![],
            current_frame: (0, 0),

            locals_window_opened: false,
            breakpoints_window_opened: false,
            frames_window_opened: false,

            breakpoints: HashMap::new(),
            open_file_dialog: None,
            gdb_logs: vec![],
            gdb_logs_opened: false,
            command_sender,
            response_receiver,
        };

        if let Some(program) = std::env::args().nth(1) {
            app.command_sender
                .send(GdbRequest::OpenExecutable(program))
                .unwrap();
        }

        app
    }

    fn open_filepicker(&mut self) {
        if let Ok(cwd) = std::env::current_dir() {
            let mut dialog = FileDialog::open_file(Some(cwd));
            dialog.open();
            self.open_file_dialog = Some(dialog);
        }
    }
}

fn breakpoint_key(file: &str, line: i32) -> String {
    format!("{}:{}", file, line)
}

fn breakpoint_icon(ui: &mut Ui, breakpoint: Option<&Breakpoint>) -> Response {
    let response = ui.allocate_response([10f32, 10f32].into(), Sense::click());

    if let Some(breakpoint) = breakpoint {
        if breakpoint.enabled {
            ui.painter().circle_filled(
                response.rect.center() + [2.0, 0.0].into(),
                5.0,
                Color32::RED,
            )
        } else {
            ui.painter().circle_stroke(
                response.rect.center(),
                5.0,
                Stroke::new(2.0, Color32::DARK_RED),
            )
        }
    }

    response
}

impl eframe::App for DebugApp {
    fn save(&mut self, _storage: &mut dyn eframe::Storage) {}

    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        ctx.request_repaint();

        while let Ok(response) = self.response_receiver.try_recv() {
            Log::debug(&format!("new gdb response: {:#?}", response));

            match response {
                GdbResponse::OpenedExecutable(main_function) => {
                    let source_file = SourceFile::open(main_function.filename.clone());
                    self.files.current_filename = Some(main_function.filename.clone());
                    self.files.opened_files.clear();
                    self.files.opened_files.push(main_function.filename.clone());
                    self.files
                        .all_files
                        .insert(main_function.filename.clone(), Some(source_file));
                    // TODO: read all source files and insert them into "files"

                    self.focus_line = Some((main_function.line, false));
                    self.breakpoints.clear();
                    self.values = serde_json::Value::Null;
                }
                GdbResponse::Log(gdb_log) => {
                    self.gdb_logs.push(gdb_log);
                }
                GdbResponse::Breakpoints(breakpoints) => {
                    self.breakpoints.clear();

                    breakpoints.into_iter().for_each(|bp| {
                        let file_line_key = breakpoint_key(&bp.filename, bp.line);
                        self.breakpoints.insert(file_line_key, bp);
                    });
                }
                GdbResponse::Running => {
                    self.command_sender
                        .send(GdbRequest::WaitForExecution)
                        .unwrap();
                }
                GdbResponse::Stopped(threads) => {
                    if let Some(thread) = threads.iter().find(|t| t.is_current) {
                        if let Some(current_frame) = thread.frames.iter().find(|f| f.is_current) {
                            self.files.switch_to(&current_frame.filename);
                            self.focus_line = Some((current_frame.line, false));
                        }
                    }

                    self.threads = threads;

                    self.command_sender.send(GdbRequest::GetLocals).unwrap();
                }
                GdbResponse::Values(values) => {
                    self.values = values;
                }
                GdbResponse::Panicing => {
                    // TODO: handle it nicely, show a message or something
                    frame.close();
                }
            }
        }

        if ctx.input().key_released(Key::Escape) {
            frame.close();
        }

        if ctx.input_mut().consume_shortcut(&ALT_1) {
            self.gdb_logs_opened = !self.gdb_logs_opened;
        }

        if ctx.input_mut().key_released(Key::L) {
            self.locals_window_opened = !self.locals_window_opened;
        }

        if ctx.input_mut().key_released(Key::B) {
            self.breakpoints_window_opened = !self.breakpoints_window_opened;
        }

        if ctx.input_mut().key_released(Key::F) {
            self.frames_window_opened = !self.frames_window_opened;
        }

        if ctx.input_mut().consume_shortcut(&CTRL_O) {
            self.open_filepicker();
        }

        if ctx.input_mut().key_released(Key::F5) {
            self.command_sender.send(GdbRequest::Run).unwrap();
        }

        if ctx.input_mut().key_released(Key::F6) {
            self.command_sender.send(GdbRequest::Continue).unwrap();
        }

        if ctx.input_mut().key_released(Key::F10) {
            self.command_sender.send(GdbRequest::Next).unwrap();
        }

        if ctx.input_mut().key_released(Key::F11) {
            self.command_sender.send(GdbRequest::Step).unwrap();
        }

        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                ui.menu_button("File", |ui| {
                    if ui.button("Open executable").clicked() {
                        self.open_filepicker();

                        ui.close_menu();
                    }

                    if ui.button("Quit").clicked() {
                        frame.close();
                    }
                });
            });

            if let Some(dialog) = &mut self.open_file_dialog {
                if dialog.show(ctx).selected() {
                    if let Some(file) = dialog.path() {
                        if let Some(file) = file.to_str() {
                            self.command_sender
                                .send(GdbRequest::OpenExecutable(file.to_string()))
                                .unwrap();
                        } else {
                            eprint!("can't read selected path");
                        }
                    }
                }
            }
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.style_mut().override_text_style = Some(egui::TextStyle::Monospace);

            if self.files.current_filename.is_none() {
                return;
            }

            let current_file = self
                .files
                .current_filename
                .as_mut()
                .expect("Check above insures this will work");

            // file tabs
            ui.horizontal(|ui| {
                self.files.opened_files.iter().for_each(|opened| {
                    ui.selectable_value(current_file, opened.clone(), filename(opened));
                });
            });
            ui.add(Separator::default().horizontal().spacing(0f32));

            // get or insert source file
            // TODO: retrace the steps and verify we need to do that here.
            // it might be the case that the correct place will be the frame stop handler,
            // then just ".expect()" will be enough
            let source_file = self
                .files
                .all_files
                .entry(current_file.clone())
                .or_insert_with(|| Some(SourceFile::open(current_file.clone())));

            if source_file.is_none() {
                *source_file = Some(SourceFile::open(current_file.clone()));
            }

            let mut source_file = source_file
                .as_mut()
                .expect("At this point file should be there");

            let vertical = egui::ScrollArea::vertical()
                .auto_shrink([false; 2])
                .scroll_offset(source_file.scroll_offset)
                .show(ui, |ui| {
                    source_file
                        .lines
                        .iter()
                        .enumerate()
                        .for_each(|(i, string)| {
                            let i = (i + 1) as i32;

                            ui.horizontal(|ui| {
                                // hightlight current line
                                let mut line_number_color = Color32::DARK_GRAY;

                                if let Some(thread) = self.threads.get(self.current_frame.0 as usize) {
                                    if let Some(frame) = thread.frames.get(self.current_frame.1 as usize) {
                                        if frame.line == i {
                                            ui.painter().rect_filled(
                                                ui.max_rect(),
                                                Rounding::default(),
                                                Color32::DARK_GRAY,
                                                );
                                            line_number_color = Color32::WHITE;
                                        }
                                    }
                                };

                                let mut breakpoint = None;

                                if let Some(filename) = self.files.current_filename.as_ref() {
                                    breakpoint = self.breakpoints.get(&breakpoint_key(filename, i));
                                }

                                let breakpoint_area = ui
                                    .horizontal(|ui| {
                                        breakpoint_icon(ui, breakpoint);

                                        let line_label =
                                            ui.colored_label(line_number_color, i.to_string());
                                        if let Some((line, scrolled)) = self.focus_line {
                                            if !scrolled && line == i {
                                                line_label.scroll_to_me(Some(egui::Align::Center));
                                                self.focus_line = Some((line, true));
                                            }
                                        }

                                        ui.min_rect()
                                    })
                                    .inner;

                                let breakpoint_area_response = ui.allocate_rect(
                                    breakpoint_area,
                                    Sense::click().union(Sense::hover()),
                                );

                                if breakpoint_area_response.hovered() {
                                    ui.output().cursor_icon = CursorIcon::PointingHand;
                                }

                                if let Some(filename) = self.files.current_filename.as_ref() {
                                    if breakpoint_area_response.clicked() {
                                        if breakpoint.is_some() {
                                            self.command_sender
                                                .send(GdbRequest::BreakpointDelete((
                                                    filename.clone(),
                                                    i,
                                                )))
                                                .unwrap();
                                        } else {
                                            self.command_sender
                                                .send(GdbRequest::BreakpointSet((
                                                    filename.clone(),
                                                    i,
                                                )))
                                                .unwrap();
                                        }
                                    }
                                }

                                Label::new(RichText::new(string).color(Color32::WHITE))
                                    .wrap(true)
                                    .ui(ui);
                            });
                        });
                });

            source_file.scroll_offset = vertical.state.offset;
        });

        egui::Window::new("Locals")
            .resizable(true)
            .default_size(Vec2::new(400f32, 300f32))
            .open(&mut self.locals_window_opened)
            .show(ctx, |ui| {
                ui.style_mut().override_text_style = Some(egui::TextStyle::Monospace);
                let max_height = frame.info().window_info.size.y - 50f32;
                let max_width = frame.info().window_info.size.x - 50f32;

                egui::ScrollArea::both()
                    .max_height(max_height)
                    .max_width(max_width)
                    .auto_shrink([false, false])
                    .show(ui, |ui| {
                        if let serde_json::Value::Object(object) = &self.values {
                            render_complex_vars(ui, object);
                        }
                    });
            });

        egui::Window::new("Gdb Logs")
            .resizable(true)
            .open(&mut self.gdb_logs_opened)
            .show(ctx, |ui| {
                ui.style_mut().override_text_style = Some(egui::TextStyle::Monospace);
                let max_height = frame.info().window_info.size.y - 100f32;
                let max_width = frame.info().window_info.size.x - 50f32;

                egui::ScrollArea::vertical()
                    .max_height(max_height)
                    .max_width(max_width)
                    .auto_shrink([false, false])
                    .stick_to_bottom(true)
                    .show(ui, |ui| {
                        for log in &self.gdb_logs {
                            match log {
                                GdbLog::Request(string) => {
                                    ui.colored_label(Color32::GRAY, string);
                                }
                                GdbLog::Response(string) => {
                                    ui.colored_label(Color32::WHITE, string);
                                }
                            }
                        }
                    });

                let response = TextEdit::singleline(&mut self.custom_command)
                    .desired_width(f32::INFINITY)
                    .ui(ui);

                if response.lost_focus() && ui.input().key_pressed(Key::Enter) {
                    self.command_sender
                        .send(GdbRequest::Custom(self.custom_command.clone()))
                        .unwrap();
                    self.custom_command.clear();
                    response.request_focus();
                }
            });

        egui::Window::new("Breakpoints")
            .resizable(true)
            .default_size(Vec2::new(200f32, 300f32))
            .open(&mut self.breakpoints_window_opened)
            .show(ctx, |ui| {
                ui.style_mut().override_text_style = Some(egui::TextStyle::Monospace);
                let max_height = frame.info().window_info.size.y - 50f32;
                let max_width = frame.info().window_info.size.x - 50f32;

                egui::ScrollArea::vertical()
                    .max_height(max_height)
                    .max_width(max_width)
                    .auto_shrink([false, false])
                    .show(ui, |ui| {
                        ui.horizontal(|ui| {
                            if ui.small_button("Delete all").clicked() {
                                self.command_sender
                                    .send(GdbRequest::BreakpointsDeleteAll)
                                    .unwrap();
                            }

                            if ui.small_button("Enable all").clicked() {
                                self.command_sender
                                    .send(GdbRequest::BreakpointsToggle(true))
                                    .unwrap();
                            }

                            if ui.small_button("Disable all").clicked() {
                                self.command_sender
                                    .send(GdbRequest::BreakpointsToggle(false))
                                    .unwrap();
                            }
                        });

                        for breakpoint in self.breakpoints.values() {
                            ui.push_id(breakpoint.number, |ui| {
                                let breakpoint_row_response = ui
                                    .horizontal(|ui| {
                                        let row_rect = ui.max_rect();
                                        let breakpoint_row_response = ui.interact(
                                            row_rect,
                                            ui.id(),
                                            Sense::click().union(Sense::hover()),
                                        );

                                        if ui.interact(row_rect, ui.id(), Sense::hover()).hovered()
                                        {
                                            ui.painter().rect_filled(
                                                row_rect,
                                                Rounding::none(),
                                                ui.visuals().faint_bg_color,
                                            );
                                            ui.output().cursor_icon = CursorIcon::PointingHand;
                                        }

                                        if ui.small_button("x").clicked() {
                                            self.command_sender
                                                .send(GdbRequest::BreakpointDelete((
                                                    breakpoint.filename.to_owned(),
                                                    breakpoint.line,
                                                )))
                                                .unwrap();
                                        }

                                        if breakpoint_icon(ui, Some(breakpoint)).clicked() {
                                            self.command_sender
                                                .send(GdbRequest::BreakpointToggle((
                                                    breakpoint.number,
                                                    !breakpoint.enabled,
                                                )))
                                                .unwrap();
                                        }

                                        ui.colored_label(
                                            Color32::WHITE,
                                            &format!(
                                                "{}:{}",
                                                filename(&breakpoint.filename),
                                                breakpoint.line
                                            ),
                                        );

                                        breakpoint_row_response
                                    })
                                    .inner;

                                if breakpoint_row_response.double_clicked() {
                                    // jump to the line on double click
                                    self.focus_line = Some((breakpoint.line, false));
                                }
                            });
                        }
                    });
            });

        egui::Window::new("Call stack")
            .resizable(true)
            .default_size(Vec2::new(200f32, 300f32))
            .open(&mut self.frames_window_opened)
            .show(ctx, |ui| {
                ui.style_mut().override_text_style = Some(egui::TextStyle::Monospace);
                let max_height = frame.info().window_info.size.y - 50f32;
                let max_width = 400f32;

                egui::ScrollArea::vertical()
                    .max_height(max_height)
                    .max_width(max_width)
                    .auto_shrink([false, false])
                    .show(ui, |ui| {

                        for (thread_index, thread) in self.threads.iter().enumerate() {
                            ui.collapsing(format!("{}:tid={} \"{}\"", thread.id, thread.tid, thread.name), |ui| {
                            for (frame_index, frame) in thread.frames.iter().enumerate() {
                                ui.push_id(frame_index, |ui| {
                                    let breakpoint_row_response = ui
                                        .horizontal(|ui| {
                                            let row_rect = ui.max_rect();

                                            // hightlight current frame row
                                            if frame.is_current && thread.is_current {
                                                ui.painter().rect_filled(
                                                    row_rect,
                                                    Rounding::none(),
                                                    Color32::DARK_GRAY,
                                                );
                                            }

                                            let breakpoint_row_response = ui.interact(
                                                row_rect,
                                                ui.id(),
                                                Sense::click().union(Sense::hover()),
                                                );

                                            if ui.interact(row_rect, ui.id(), Sense::hover()).hovered()
                                            {
                                                ui.painter().rect_filled(
                                                    row_rect,
                                                    Rounding::none(),
                                                    ui.visuals().faint_bg_color,
                                                    );
                                                ui.output().cursor_icon = CursorIcon::PointingHand;
                                            }

                                            if let Some(function) = frame.function.as_ref() {
                                                ui.colored_label(Color32::WHITE, function);
                                            }

                                            ui.with_layout(Layout::top_down(egui::Align::Max), |ui| {
                                                ui.colored_label(
                                                    Color32::WHITE,
                                                    &format!(
                                                        "{}:{}",
                                                        filename(&frame.filename),
                                                        frame.line
                                                        ),
                                                        );
                                            });

                                            breakpoint_row_response
                                        })
                                    .inner;

                                    if breakpoint_row_response.double_clicked() && !frame.is_library {
                                        // jump to the line on double click
                                        self.command_sender
                                            .send(GdbRequest::SelectFrame(thread.id, frame.level))
                                            .unwrap();

                                        self.files.switch_to(&frame.filename);
                                        self.focus_line = Some((frame.line, false));
                                        self.current_frame = (thread_index as i32, frame_index as i32);
                                    }
                                });
                            }
                            });
                        }
                    });
            });
    }
}
