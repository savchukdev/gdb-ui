/*
 * This file is part of rust-gdb.
 *
 * rust-gdb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rust-gdb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rust-gdb.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::str;

#[derive(Debug)]
pub enum Record {
    Result(MessageRecord<ResultClass>),
    Async(AsyncRecord),
    Stream(StreamRecord)
}

#[derive(Debug)]
pub struct MessageRecord<ClassT> {
    pub token: Option<String>,
    pub class: ClassT,
    pub content: Vec<Variable>,
}

#[derive(Debug, PartialEq)]
pub enum ResultClass {
    Done,
    Running,
    Connected,
    Error,
    Exit
}

#[derive(Debug, PartialEq)]
pub enum AsyncClass {
    Stopped,
    BreakpointCreated,
    BreakpointDeleted,
    Other
}

#[derive(Debug)]
pub enum AsyncRecord {
    Exec(MessageRecord<AsyncClass>),
    Status(MessageRecord<AsyncClass>),
    Notify(MessageRecord<AsyncClass>),
}

#[derive(Debug)]
pub enum StreamRecord {
    Console(String),
    Target(String),
    Log(String),
}

#[derive(Debug)]
pub struct Variable {
    pub name: String,
    pub value: Value
}

#[derive(Debug)]
pub enum Value {
    String(String),
    Integer(i32),
    VariableList(Vec<Variable>),
    ValueList(Vec<Value>),
}

impl str::FromStr for ResultClass {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "done" => Ok(ResultClass::Done),
            "running" => Ok(ResultClass::Running),
            "connected" => Ok(ResultClass::Connected),
            "error" => Ok(ResultClass::Error),
            "exit" => Ok(ResultClass::Exit),
            _ => Err("unrecognized result class".to_string()),
        }
    }
}

impl str::FromStr for AsyncClass {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "stopped" => Ok(AsyncClass::Stopped),
            "breakpoint-created" => Ok(AsyncClass::BreakpointCreated),
            "breakpoint-deleted" => Ok(AsyncClass::BreakpointDeleted),
            _ => Ok(AsyncClass::Other),
        }
    }
}
