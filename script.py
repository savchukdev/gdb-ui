import json
import base64

def encode(data):
    return base64.b64encode(json.dumps(data).encode('utf-8')).decode('utf-8')

def format_breakpoint(bp):
    _, [location] = gdb.decode_line(bp.location)

    return {
        'number': bp.number,
        'enabled': bp.enabled,
        'filename': location.symtab.fullname(),
        'line': location.line
    }

def format_breakpoints():
    breakpoints = filter(lambda bp: bp.is_valid(), gdb.breakpoints())
    breakpoints = map(lambda bp: format_breakpoint(bp), breakpoints)
    # breakpoints = list(breakpoints or []).sort(key=lambda bp: bp['number'])

    return list(breakpoints)

class MIBreakpointToggle(gdb.MICommand):
    """Toggle one breakpoint"""

    def __init__(self, name):
        super(MIBreakpointToggle, self).__init__(name)

    def invoke(self, argv):
        if len(argv) != 2:
            return

        if argv[1] == "on":
            gdb.execute(f"enable {argv[0]}")
        elif argv[1] == "off":
            gdb.execute(f"disable {argv[0]}")

        return { 'breakpoints': encode(format_breakpoints()) }

class MIBreakpointsToggle(gdb.MICommand):
    """Toggle all breakpoints"""

    def __init__(self, name):
        super(MIBreakpointsToggle, self).__init__(name)

    def invoke(self, argv):
        if len(argv) != 1:
            return

        if argv[0] == "on":
            gdb.execute("enable")
        elif argv[0] == "off":
            gdb.execute("disable")

        return { 'breakpoints': encode(format_breakpoints()) }

class MIAllBreaksDelete(gdb.MICommand):
    """Delete all breakpoints"""

    def __init__(self, name):
        super(MIAllBreaksDelete, self).__init__(name)

    def invoke(self, argv):
        gdb.execute("delete")

        return { 'breakpoints': encode(format_breakpoints()) }

class MIBreakDelete(gdb.MICommand):
    """Delete a single breakpoint"""

    def __init__(self, name):
        super(MIBreakDelete, self).__init__(name)

    def invoke(self, argv):
        if len(argv) != 1:
            return

        gdb.execute(f"clear {argv[0]}")

        return { 'breakpoints': encode(format_breakpoints()) }

class MIBreakSet(gdb.MICommand):
    def __init__(self, name):
        super(MIBreakSet, self).__init__(name)

    def invoke(self, argv):
        if len(argv) != 1:
            return

        gdb.Breakpoint(argv[0])

        return { 'breakpoints': encode(format_breakpoints()) }

class MIFindMainFunc(gdb.MICommand):
    def __init__(self, name):
        super(MIFindMainFunc, self).__init__(name)

    def invoke(self, argv):
        try:
            _, (locations) = gdb.decode_line("main")
            location = locations[0]

            return {
                'main_function': encode(
                    {'filename': location.symtab.fullname(), 'line': location.line}
                )
            }
        except:
            pass


class MILocals(gdb.MICommand):
    def __init__(self, name):
        super(MILocals, self).__init__(name)

    def invoke(self, argv):
        frame = None
        try:
            frame = gdb.selected_frame()
        except:
            return {}

        block = frame.block()

        result = {}
        for symbol in block:
            value = symbol.value(frame)

            result[symbol.name] = self.process_value(value)

        return {'locals': encode(result)}

    def process_value(self, value):
        result = {}
        result['type'] = str(value.type)

        if value.address:
            result['address'] = str(value.address)

        is_string = (str(value.type.strip_typedefs()) == "char *" or
                     value.type.code == gdb.TYPE_CODE_STRING)

        if (value.type.code == gdb.TYPE_CODE_PTR and
            not is_string and
            not value.type.target().code == gdb.TYPE_CODE_VOID and
            value != 0):
            value = value.dereference()

        if value.type.is_scalar:
            result['scalar'] = True

            if value.type.code == gdb.TYPE_CODE_PTR and value == 0:
                result['null'] = True
                result['value'] = 0
            else:
                code = value.type.code
                if code == gdb.TYPE_CODE_INT:
                    result['value'] = int(value)
                elif code == gdb.TYPE_CODE_FLT:
                    result['value'] = float(value)
                elif code == gdb.TYPE_CODE_BOOL:
                    result['value'] = bool(value)
                elif code == gdb.TYPE_CODE_STRING or is_string:
                    try:
                        result['value'] = str(value.string())
                    except:
                        result['value'] = None
                else:
                    result['value'] = str(value)

            return result

        result['fields'] = {}
        for field in value.type.fields():
            result['fields'][field.name] = self.process_value(value[field.name])

        return result


MILocals("-my-locals")
MIFindMainFunc("-my-find-main-func")
MIBreakSet("-my-break-set")
MIBreakDelete("-my-break-delete")
MIAllBreaksDelete("-my-breaks-delete")
MIBreakpointsToggle("-my-breaks-toggle")
MIBreakpointToggle("-my-break-toggle")

def collect_frame_into(frame):
    info = {}
    func = frame.function()

    if func:
        symtab_and_line = frame.find_sal()

        info['function'] = func.name
        info['filename'] = symtab_and_line.symtab.fullname()
        info['line'] = symtab_and_line.line
        info['is_library'] = False
    else:
        info['is_library'] = True
        info['function'] = '??'
        info['filename'] = gdb.solib_name(frame.pc())
        info['line'] = 0


    info['level' ] = frame.level()
    info['pc'] = frame.pc()
    info['is_current'] = frame == gdb.selected_frame()

    return info

def collect_frames():
    frames = []
    try:
        current_frame = gdb.selected_frame()
    except:
        return frames

    # collect info on all newer frames
    newer_frame = current_frame.newer()
    while newer_frame:
        frames.insert(0, collect_frame_into(newer_frame))
        newer_frame = newer_frame.newer()

    # collect info on all older frames
    while current_frame:
        frames.append(collect_frame_into(current_frame))
        current_frame = current_frame.older()

    return frames


def stop_handler(event):
    current_thread = gdb.selected_thread()

    threads = []

    for thread in gdb.selected_inferior().threads():
        thread.switch()
        if not thread.is_valid():
            continue

        info = {}
        info['name'] = thread.name
        info['id'] = thread.num
        _, info['tid'], _ = thread.ptid
        info['frames'] = collect_frames()
        info['is_current'] = thread == current_thread

        threads.append(info)

    current_thread.switch()

    print("stopped", encode(threads), end='', flush=True)

gdb.events.stop.connect(stop_handler)
